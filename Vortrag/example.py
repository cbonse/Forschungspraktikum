import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
import cv2

def get_ax(x,y,img):
    fig = plt.figure(figsize=(16,12))
    ax = fig.add_subplot(111)
    ax.plot(x,y,c="r",linestyle="--")
    ax.imshow(img)
    ax.set_xlim((575,1250))
    ax.set_ylim((550,260))
    return ax

if __name__ == "__main__":
    x = np.array([640,690,715,740,765,775,860,940,980,990,1050,1150])
    y = np.array([400,360,350,370,430,495,500,500,455,405,350,350])
    n = 10
    img = cv2.imread("./mensa_bearbeitet.png")
    ax = get_ax(x,y,img)
    ax.scatter(x,y,c="r",s=100)
    plt.tight_layout()
    plt.savefig("naive1.png")
    ax = get_ax(x,y,img)
    rx,ry = [],[]
    for ix, iy in zip(x,y):
        rd_x = np.random.normal(ix,5,n)
        rd_y = np.random.normal(iy,5,n)
        rx.append(rd_x)
        ry.append(rd_y)
        ax.scatter(rd_x,rd_y)
    ax.scatter(x,y,c="r",s=100)
    plt.tight_layout()
    plt.savefig("naive2.png")
    rx = np.array(rx).T
    ry = np.array(ry).T
    ax = get_ax(x,y,img)
    for i in range(n):
        ax.plot(rx[i],ry[i])
        ax.scatter(rx,ry)
    plt.tight_layout()
    plt.savefig("naive3.png")
    ax = get_ax(x,y,img)
    t = np.arange(10)
    ti = np.linspace(t.min(),t.max(),20)
    print(len(t))
    print(len(rx))
    for i in range(n):
        fx = interp1d(t,rx.T,kind="cubic")
        fy = interp1d(t,ry.T,kind="cubic")
        ax.plot(fx(ti),fy(ti))


    plt.show()
