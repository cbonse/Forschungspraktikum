import numpy as np
from matplotlib import pyplot as plt

from sklearn.gaussian_process.kernels import RBF

if __name__ == "__main__":
    plot = False

    n_samples = 5
    dim = 100
    mean = np.zeros((dim))

    x_std = np.load("x_std_mean.npy").reshape(dim, 1)
    y_std = np.load("y_std_mean.npy")#.reshape(100, 1)

    # x_std = np.linspace(0,100,100).reshape(100, 1)
    # x_std[50:,0] = np.linspace(50,0,50)

    if plot:
        plt.title("Variances")
        plt.plot(x_std,c="g",label="$\sigma_x$")
        plt.plot(y_std,c="r",label="$\sigma_y$")
        plt.legend()
        plt.show()

    k = RBF(length_scale=1.0)(x_std)
    # plt.imshow(k)
    # plt.show()

    samples = np.random.multivariate_normal(mean, k, size=n_samples)

    for i in range(n_samples):
        plt.plot(range(dim), samples[i], linewidth=3)
    plt.show()
