import os
import numpy as np
import pandas as pd
from scipy.optimize import brentq
from scipy.interpolate import interp1d

import h5py
from shapely.geometry import LineString

def filter_by_origin(path, goal, target=None):
    r"""Filter Trajectories by their origin.

    Filters the hdf5-dataset specified by `path` and makes a copy containing
    only trajectories originating at a given `goal`.

    Trajectories starting at the /Unassigned/-goal are ignored.

    Parameters
    ----------
    path : *str*
        Valid path to base HDF5-dataset.
    goal : *str*
        Goal from which all trajectories in the reduced dataset originate.
    target : *str*
        Specific path at which to create the copy of the dataset. If no
        value is passed, the dataset will be named after the original,
        with the addition of "_reduced", e.g.: \"original.hdf5\" would become
        \"original_reduced.hdf5\".

    """
    if not os.path.exists(path):
        raise ValueError("Invalid Path specified.")
    try:
        with open(path, "r") as f:
            pass
    except PermissionError:
        raise PermissionError("Could not read from file %s"%path)
    try:
        int_goal = int(goal)
    except ValueError:
        raise ValueError("Goal could not be converted to integer.")
    if not target:
        if path[-4] == ".":
            target = path[:-4]+"_reduced.hdf5"
        else:
            target = path+"_reduced.hdf5"

    count = 0
    with h5py.File(path, "r") as f:
        with h5py.File(target, "w") as f_out:
            f_out.attrs["Filtered by Origin"] = "Only Trajectories originating at Goal %s"%goal
            for grp in f:
                if grp.isdigit():
                    for trajectory in f[grp]:
                        origin_index = f[grp+"/"+trajectory].attrs["Origin"]
                        if origin_index == goal:
                            f.copy(grp+"/"+trajectory, f_out, name=grp+"/"+trajectory)
                            count += 1
            f_out.attrs["TotalNrTrajectories"] = count

def filter_by_length(path, length, target=None):
    r"""Filter trajectories by their length.

    Filters the hdf5-dataset specified by `path` and makes a copy containing
    only trajectories shorter than `length`.
    The rationale behind this is that very long trajectories are often the
    weird ones throwing off the classification, so it's better to leave them
    out of the whole classification.

    Trajectories starting at the /Unassigned/-goal are ignored.

    Trajectories of only one point are also ignored!

    Parameters
    ----------
    path : *str*
        Valid path to base HDF5-dataset.
    length : *int*
        Maximum length of trajectories to be considered
    target : *str*
        Specific path at which to create the copy of the dataset. If no
        value is passed, the dataset will be named after the original,
        with the addition of "_reduced", e.g.: \"original.hdf5\" would become
        \"original_shorter.hdf5\".

    """
    if not os.path.exists(path):
        raise ValueError("Invalid Path specified.")
    try:
        with open(path, "r") as f:
            pass
    except PermissionError:
        raise PermissionError("Could not read from file %s"%path)
    try:
        int_length = int(length)
    except ValueError:
        raise ValueError("Goal could not be converted to integer.")
    if not target:
        if path[-4] == ".":
            target = path[:-4]+"_shorter.hdf5"
        else:
            target = path+"_shorter.hdf5"

    count = 0
    with h5py.File(path, "r") as f:
        with h5py.File(target, "w") as f_out:
            f_out.attrs["Filtered by Length"] = "Only Trajectories shorter than %i"%length
            for grp in f:
                if grp.isdigit():
                    for trajectory in f[grp]:
                        path = grp+"/"+trajectory
                        actual_length = len(f[path+"/Positions"][:])
                        if 2 <= actual_length <= length:
                            f.copy(path, f_out, name=path)
                            count += 1
            f_out.attrs["TotalNrTrajectories"] = count

def filter_by_number(path, number, target=None):
    r"""Filter trajectories by number of trajectories starting at a goal.

    Filters the hdf5-dataset specified by `path` and makes a copy containing
    only `number ` trajectories per goal.

    The selection of goals is done randomly.

    Trajectories starting at the /Unassigned/-goal are ignored.

    Parameters
    ----------
    path : *str*
        Valid path to base HDF5-dataset.
    number : *int*
        Maximum number of trajectories per goal to be considered
    target : *str*
        Specific path at which to create the copy of the dataset. If no
        value is passed, the dataset will be named after the original,
        with the addition of "_reduced", e.g.: \"original.hdf5\" would become
        \"original_fewer.hdf5\".

    """
    if not os.path.exists(path):
        raise ValueError("Invalid Path specified.")
    try:
        with open(path, "r") as f:
            pass
    except PermissionError:
        raise PermissionError("Could not read from file %s"%path)
    try:
        int_number = int(number)
    except ValueError:
        raise ValueError("Goal could not be converted to integer.")

    count = 0
    with h5py.File(path, "r") as f:
        with h5py.File(target, "w") as f_out:
            f_out.attrs["Filtered by Length"] = "Only a maximum of "+\
                                            "%i Trajectories per Goal"%number
            goals = [el for el in f.keys() if el.isdigit()]
            for grp in goals:
                actual_nr = len(f[grp])
                if actual_nr <= number:
                    choice = range(actual_nr)
                else:
                    choice = np.random.choice(actual_nr, number, replace=False)
                for i, trajectory in enumerate(f[grp]):
                    if i in choice:
                        f.copy(grp+"/"+trajectory, f_out, name=grp+"/"+trajectory)
                        count += 1
            f_out.attrs["TotalNrTrajectories"] = count

def filter_trajectory(data, kind=None, windowsize=3):
    r"""Filter a single trajectories for outlier-removal.

    Filteres a trajectory to remove outliers by turning it over to `pandas`
    rolling_mean or rolling_median functions.

    The resulting will be shorter because of the windowing!

    Parameters
    ----------
    data : *array_like*
        (n_points, 2)-Array containing the Position data.
    kind : *str*
        Specifies the kind of filter applied to a trajectory. Possible
        values are 'mean' to apply a `pandas.rolling_mean`, 'median' to apply
        a `pandas.rolling_median` or just nothing to simply return the
        unfiltered trajectory
    windowsize : *int, optional*
        Optional windowsize for use in conjunction with a filter.

    Returns
    -------
    filtered_trajectory  : *array_like*
        Array containing either the filtered trajectory or the original if
        `kind` was not supplied.

    """
    if not isinstance(data, np.ndarray):
        raise ValueError("Invalid input data.")
    if data.shape[1] != 2:
        raise ValueError("Invalid input data-shape. Trajectory must be "+\
                         "in the shape of (n_points, 2)")
    if kind:
        if isinstance(kind, str):
            if kind.lower() not in ["median", "mean"]:
                raise ValueError("Invalid Filter-kind specified.")
        else:
            raise ValueError("Invalid Filter-kind specified.")
        try:
            int_window = int(windowsize)
        except ValueError:
            raise ValueError("Invalid windowsize specified.")
        if not (1 <= int_window <= data.shape[0]):
            raise ValueError("Invalid windowsize specified.")
    if not kind:
        return data
    elif kind.lower() == "median":
        return pd.rolling_median(data, windowsize)[windowsize-1:]
    elif kind.lower() == "mean":
        return pd.rolling_mean(data, windowsize)[windowsize-1:]

def adjust_trajectory_length(trajectory, length):
    r""" Interpolate a trajectory to have a given number of points.

    Linearily interpolates a given `trajectory` so it has the exact
    `length`. Useful since for all machine-learning processes, the
    number of features is ifx.

    Parameters
    ----------
    trajectory : *array_like*
        (n_points, 2)-Array containing the position data of a trajectory.
    length : *int*
         Desired length of  the trajectory

    Returns
    -------
    interpolated_trajectory : *array_like*
        Trajectory interpolated so it has the given length.

    """
    if not isinstance(trajectory, np.ndarray):
        raise ValueError("Invalid input data.")
    if trajectory.shape[1] != 2:
        raise ValueError("Invalid input data-shape. Trajectory must be "+\
                         "in the shape of (n_points, 2)")
    try:
        int_length = int(length)
    except:
        raise ValueError("Invalid length-value.")
    if int_length <= 0:
        raise ValueError("Invalid length-value. The length must be positive.")
    elif int_length == trajectory.shape[0]:
        return trajectory

    t = np.arange(trajectory.shape[0])
    ti = np.linspace(t.min(), t.max(), length)
    fx = interp1d(t, trajectory[:,0])
    fy = interp1d(t, trajectory[:,1])
    xi = fx(ti)
    yi = fy(ti)
    return np.vstack([xi, yi]).T

def dp_algorithm(tolerance, trajectory):
    r"""Douglas-Peucker Algorithm for trajectory-tegmentation.

    This Algorithm is used to shorten a given trajectory in `process_trajectory`
    without losing too much information.

    Since this is just a wrapper-function it is not covered in the test-suite.
    """
    filtered = LineString(trajectory).simplify(tolerance, \
                                            preserve_topology=False)
    return np.array(filtered.coords[:])

def dp_cost_function(tolerance, trajectory):
    r"""Cost-Function for finding the optimal dp_algorithm tolerance.

    This function is used by the brentq-optimizer in `process_trajectory`.

    """
    return len(dp_algorithm(tolerance, trajectory))-trajectory.shape[0]

def process_trajectory(trajectory, length, filter_parameter):
    r"""Main trajectory-processing pipeline.

    This is the main function for processing a trajectory so that it has the
    given `length`.

    First the trajectory is filtered to remove outliers, the way of which is
    specified in the `filter_parameter`.

    If the give `trajectory` is too long, it is segmented by using the
    Douglas-Peucker Algorithm. The optimal tolerance for the Algorithm is
    determined by using the `scipy.brentq`-optimizer. Should the trajectory
    still not have the right number of points (when the optimal tolerance is
    minimally off, usually the number of points in the trajectory are then
    also off by just a few), the trajectory is linearily interpolated.

    If the `trajectory` is too short on the other hand, it is directly
    linearily interpolated and returned.

    TODO: include the tests for this function.

    Parameters
    ----------
    trajectory : *array_like*
        (n_points, 2)-Array containing the Position data of a Trajectory.
    length : *int*
         Desired length of  the trajectory
    filter_parameter : *tuple*
        (filter-kind, windowsize) Parameter-Tuple for the `filter_trajectory`
        function.

    Returns
    -------
    processed : *array_like*
        (n_points, 2)-Array containing the processed trajectory, guaranteed
        to be of a given `length`.

    """
    filtered = filter_trajectory(trajectory)
    if len(filtered) > length:
        try:
            optimal_tolerance = brentq(dp_cost_function, a=0, b=100, args=(filtered,))
            segmented = dp_algorithm(optimal_tolerance, filtered)
            if len(segmented) != length:
                return adjust_trajectory_length(segmented, length)
            else:
                return segmented.T.ravel()
        except ValueError:
            return adjust_trajectory_length(filtered, length)
    else:
        return adjust_trajectory_length(filtered, length)
